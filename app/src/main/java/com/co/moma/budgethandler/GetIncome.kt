package com.co.moma.budgethandler

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.widget.EditText
import android.widget.LinearLayout
import com.co.moma.budgethandler.Model.Income
import kotlinx.android.synthetic.main.activity_get_income.*

class GetIncome : AppCompatActivity() {

    private var m_income = Income()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_get_income)

        bt_add_another.setOnClickListener {
            val view = layoutInflater.inflate(R.layout.number_entry, null)
            ly_income.addView(view, 0)
        }
        bt_ok.setOnClickListener {

        }
    }
}
