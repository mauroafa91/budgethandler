package com.co.moma.budgethandler.Model

import java.io.Serializable

class Income: Serializable {

    private var sIncomeAlias: String = ""
    private var dIncomeAmount: Float = 0.0f

    constructor()

    constructor(sIncomeAlias: String, dIncomeAmount: Float) {
        this.sIncomeAlias = sIncomeAlias
        this.dIncomeAmount = dIncomeAmount
    }
}