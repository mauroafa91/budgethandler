package com.co.moma.budgethandler

import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.co.moma.budgethandler.ViewModel.UserViewModel
import kotlinx.android.synthetic.main.activity_setup_income.*

class Setup : AppCompatActivity() {

    private lateinit var m_eStep: Step
    private var m_bOverallResult = true
    private var m_sErrorText = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setup_income)
        val userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
        m_eStep = Step.eInitialized

        bt_next.setOnClickListener {
            getNextStep()
        }
    }

    override fun onStart() {
        super.onStart()

        doStep()
    }

    private fun getNextStep() {
        if(!m_bOverallResult) {
            //Something went wrong
            Toast.makeText(this, m_sErrorText, Toast.LENGTH_LONG).show()
        }
        else {
            //We are still ok, lets see what we can do
            when(m_eStep) {

                Step.eInitialized -> TODO()
                Step.eIncome -> {
                    m_bOverallResult = stepIncome()
                }
                Step.eCreditCard -> TODO()
                Step.eRegularExpenses -> TODO()
                Step.eDone -> TODO()
            }
        }
    }

    private fun doStep() {
        when(m_eStep) {
            Step.eInitialized -> {
                //First, we need to show the first window so the user
                //Can't view what is going happen
                m_eStep = Step.eIncome
            }
            Step.eIncome -> TODO()
            Step.eCreditCard -> TODO()
            Step.eRegularExpenses -> TODO()
            Step.eDone -> TODO()
        }
    }

    private fun stepIncome(): Boolean {
        startActivityForResult(Intent(this, GetIncome::class.java), RC_INCOME)
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode) {
            RC_INCOME -> {
                if(resultCode == Activity.RESULT_OK) {

                }
            }
        }
    }

    companion object {
        private var RC_INCOME = 1
    }

    enum class Step {
        eInitialized,
        eIncome,
        eCreditCard,
        eRegularExpenses,
        eDone
    }
}
